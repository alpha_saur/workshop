var gulp = require('gulp');
var less = require('gulp-less');
var browserSync = require('browser-sync').create();
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var pkg = require('./package.json');
var concat = require('gulp-concat');

// Set the banner content
// var banner = ['/*!\n',
//     ' * Start Bootstrap - <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n',
//     ' * Copyright 2013-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
//     ' * Licensed under <%= pkg.license.type %> (<%= pkg.license.url %>)\n',
//     ' */\n',
//     ''
// ].join('');

var paths = {
    sass: './public-src/scss/**/*.scss',
    css_src: './sources/css/**/*.css',
    css: './public/css',
    js_src: './sources/js/**/*.js',
    js: './public/js',
    img_src: './sources/images/**/*',
    img: './public/images',
    fonts_src: './sources/assets/fonts/*',
    fonts: './public/fonts'
};

gulp.task('minstyles', function () {
    return gulp.src([
        './sources/assets/css/main.css',
        './sources/assets/css/noscript.css',
        paths.css_src
    ])
        .pipe(concat('minstyles.min.css'))
        .pipe(minifyCss())
        .pipe(rename('minstyles.min.css'))
        .pipe(gulp.dest(paths.css));
});

gulp.task('minscripts', function () {
    return gulp.src([
        // Core
        './sources/assets/js/jquery.min.js',
        './sources/assets/js/skel.min.js',
        './sources/assets/js/util.js',
        './sources/assets/js/main.js',
        // Plugins
        // Scripts
        paths.js_src
    ])
        .pipe(concat('minscripts.min.js'))
        .pipe(uglify())
        .pipe(rename('minscripts.min.js'))
        .pipe(gulp.dest(paths.js));
});

// Copy vendor libraries from /node_modules into /vendor
gulp.task('copy', function () {
    // gulp.src(['node_modules/bootstrap/dist/**/*', '!**/npm.js', '!**/bootstrap-theme.*', '!**/*.map'])
    // .pipe(gulp.dest('./public/vendor/bootstrap'))



    gulp.src([paths.fonts_src])
        .pipe(gulp.dest(paths.fonts))


    gulp.src([paths.img_src])
        .pipe(gulp.dest(paths.img))
})

// Run everything
gulp.task('default', ['minscripts', 'minstyles', 'copy']);

// Configure the browserSync task
gulp.task('browserSync', function () {
    browserSync.init({
        server: {
            baseDir: ''
        },
    })
})

// Dev task with browserSync
gulp.task('dev', ['browserSync', 'less', 'minify-css', 'minify-js'], function () {
    gulp.watch('less/*.less', ['less']);
    gulp.watch('css/*.css', ['minify-css']);
    gulp.watch('js/*.js', ['minify-js']);
    // Reloads the browser whenever HTML or JS files change
    gulp.watch('*.html', browserSync.reload);
    gulp.watch('js/**/*.js', browserSync.reload);
});
