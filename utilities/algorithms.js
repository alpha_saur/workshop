const crypto = require('crypto')

module.exports.genericHashing = function (algorithm, secret, message, digest, callback) {
  console.log(algorithm, secret, message, digest)
  var result = {hash: '',error: null}
  try {
    if (secret) {
      result.hash = crypto.createHmac(algorithm, secret)
        .update(message)
        .digest(digest) // hex , base64
      callback(result)
    }else {
      result.hash = crypto.createHash(algorithm, secret)
        .update(message)
        .digest(digest) // hex , base64
      callback(result)
    }
  } catch(error) {
    result.error = error.message
    callback(result)
  }
}

module.exports.genericEncryption = function (algorithm, action, secret, message, digest, callback) {
  console.log(algorithm, action, secret, message, digest)
  var result = {hash: '',error: null}
  try {
    if (action == 'encrypt') {
      const cipher = crypto.createCipher(algorithm, secret)
      var encrypted = ''
      cipher.on('readable', function () {
        const data = cipher.read()
        if (data)
          encrypted += data.toString(digest)
      })
      cipher.on('end', function () {
        result.hash = encrypted
        callback(result)
      })

      cipher.write(message)
      cipher.end()
    }else {
      const decipher = crypto.createDecipher(algorithm, secret)

      var decrypted = ''
      decipher.on('readable', function () {
        const data = decipher.read()
        if (data)
          decrypted += data.toString('utf8')
      })
      decipher.on('end', function () {
        result.hash = decrypted
        callback(result)
      })

      decipher.write(message, digest)
      decipher.end()
    }
  } catch(error) {
    result.error = 'Invalid Algorithem'
    console.log(error.message)
    callback(result)
  }
}
